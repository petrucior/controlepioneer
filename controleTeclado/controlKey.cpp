/**
 * \brief Arquivo que possibilita o controle via teclado
 */

#include <iostream>
#include "/usr/local/Aria/include/Aria.h"

using namespace std;

int main(int argc, char** argv){
	// Iniciando o Aria
	Aria::init();
	// Iniciando o conector de conexão
	ArSimpleConnector connector(&argc, argv);
	// Iniciando o robô
  	ArRobot robot;

	// Variáveis de velocidades
	double vEsq = 0.0;
	double vDir = 0.0;
	double vStop = -1.0;

	// Verificar se conseguiu conectar ao robô
	// Connect to robot, run devices in background threads, and connect to laser.
	if(!connector.connectRobot(&robot)) {
		Aria::logOptions();
		Aria::exit(1);
	}

	// Inicia o processamento em background. O parametro true significa que se a conexão é perdida, então o loop termina
	robot.runAsync(true);

	// Pegando informações sobre o robô
	robot.lock();
	ArLog::log(ArLog::Normal, "simpleConnect: Pose=(%.2f,%.2f,%.2f), Trans. Vel=%.2f, Battery=%.2fV", 
	robot.getX(), robot.getY(), robot.getTh(), robot.getVel(), robot.getBatteryVoltage());
	robot.unlock();

	while ((vEsq != vStop) && (vDir != vStop)){
		// Pega os dados de velocidade das rodas
		std::cout << ("\nDigite a velocidade para a roda esquerda\n");
		std::cin >> vEsq;
		std::cout << ("Digite a velocidade para a roda direita\n");
		std::cin >> vDir;

		robot.lock();
		robot.enableMotors();
		robot.setVel2(vEsq, vDir);
		robot.unlock();
		ArUtil::sleep(5000);
	}

	// Pegando informações sobre o robô
	robot.lock();
	ArLog::log(ArLog::Normal, "simpleConnect: Pose=(%.2f,%.2f,%.2f), Trans. Vel=%.2f, Battery=%.2fV", 
	robot.getX(), robot.getY(), robot.getTh(), robot.getVel(), robot.getBatteryVoltage());
	robot.unlock();

	// Finalizando a thread do robô
	robot.stopRunning();
	
	// Esperando a thread parar
	robot.waitForRunExit();

	// Saindo do Aria
	Aria::exit();
	return 0;
}
