#include <iostream>

class Controle{
	private: 
		double erro_integral_anterior;
		double h;
		double erro_anterior;

	public:
	Controle(){
		erro_integral_anterior = 0.0;
		h = 0.1;
		erro_anterior = 0.0;
	}

	double proporcional(double erro, double kp){
		return (erro * kp);
    	}
    
    	double integrativo(double erro, double ki) {
		return (erro_integral_anterior + (ki*h*erro));
    	}

	double derivativo(double erro, double kd) {
		return ((kd * (erro-erro_anterior))/h);
    	}

	double pd(double erro, double kp, double kd){
		double aux = (proporcional(erro,kp) + derivativo(erro,kd));
		erro_anterior = erro;
		return aux;
    	}

	double pi(double erro, double kp, double ki) {
		double integrado = integrativo(erro,ki);
		erro_integral_anterior = erro_integral_anterior + integrado;
		double aux = (proporcional(erro,kp) + integrado);
		//erro_integral_anterior = integrado;
		return aux;
    	}

	double pid(double erro, double kp, double ki, double kd) {
		double integrado = integrativo(erro,ki);
		double aux = (proporcional(erro,kp) + integrado + derivativo(erro,kd));
		erro_integral_anterior = integrado;
		erro_anterior = erro;
		return aux;
    	}
};
