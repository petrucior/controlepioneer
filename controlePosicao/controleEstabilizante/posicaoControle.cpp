/**
 * \brief Arquivo que possibilita o controle via pid
 */

#include <iostream>
#include "/usr/local/Aria/include/Aria.h"
#include "/usr/local/Aria/include/ArSick.h"
#include "Controle.cpp"	/* controle */
#include <math.h>       /* atan, sqrt, pow */

int main(int argc, char** argv){
	// Iniciando o Aria
	Aria::init();
	// Iniciando o conector de conexão
	ArSimpleConnector connector(&argc, argv);
	// Iniciando o robô
  	ArRobot robot;

	// Posições
	double xFinal = -1000.0;
	double yFinal = 1000.0;
	double x = 0.0;
	double y = 0.0;

	// Velocidades
	double pidAngular = 0.0;
	double pidLinear = 0.0;

	// Angulo
	double ang = 0.0;
	double angularDesejado = 0.0;

	// Variação de posição
	double deltaX = 0.0;
	double deltaY = 0.0;
	double linearDesejado = 0.0;

	// Variação do angulo e posição
	double deltaAngular = 0.0;
	double deltaLinear = 0.0;

	Controle* c = new Controle();

	// Verificar se conseguiu conectar ao robô
	// Connect to robot, run devices in background threads, and connect to laser.
	if(!connector.connectRobot(&robot)) {
		Aria::logOptions();
		Aria::exit(1);
	}

	// Inicia o processamento em background. O parametro true significa que se a conexão é perdida, então o loop termina
	robot.runAsync(true);

	// Pegando informações sobre o robô antes do controle
	robot.lock();
	ArLog::log(ArLog::Normal, "Posicao Inicial: Pose=(%.2f,%.2f,%.2f), Trans. Vel=%.2f, Battery=%.2fV", 
	robot.getX(), robot.getY(), robot.getTh(), robot.getVel(), robot.getBatteryVoltage());
	robot.unlock();

	bool chegou = false;

	while ( !chegou ){
		// Posições atuais
		robot.lock();
		x = robot.getX();
		y = robot.getY();
		robot.unlock();

		// Angulo
		robot.lock();
		//ang = robot.getOdometerDegrees();
		ang = robot.getTh();
		robot.unlock();

		// Calculando as diferenças de posições
		deltaX = xFinal - x;
		deltaY = yFinal - y;

		// Calculando o angulo desejado		
		angularDesejado = atan2(deltaY, deltaX)*57.3;
		
		// Calculando a diferença entre o angulo atual e o desejado
		deltaAngular = ang - angularDesejado;
		
		std::cout << (ang) << std::endl;
		std::cout << (angularDesejado) << std::endl;
		std::cout << (deltaAngular) << std::endl;

		// Tensão angular
		//pidAngular = c->pid(deltaAngular, 1.5, 0.0, 0.0);
		pidAngular = c->proporcional(deltaAngular, 1.5);

		// Calculando a hipotenusa entre as posições x e y
		linearDesejado = sqrt(pow(deltaX,2.0) + pow(deltaY, 2.0));

		// Calculando a diferente entre a posição atual e a desejada - o deltaAngular deve está em radianos
		deltaLinear = linearDesejado * cos(deltaAngular/57.3);

		// Tensão linear
		//pidLinear = c->pid(deltaLinear, 0.5, 0.0, 0.06);
		//pidLinear = c->pid(deltaLinear, 1.0, 0.1, 0.0);
		pidLinear = c->pid(deltaLinear, 0.6, 0.0, 0.5);

		// Desacoplador
		robot.lock();
		robot.enableMotors();
		robot.setVel2(pidLinear + pidAngular, pidLinear - pidAngular);
		//robot.setVel2(20.0, -20.0);
		robot.unlock();
		ArUtil::sleep(100);

		// Pegando informações sobre o robô após o controle
		robot.lock();
		ArLog::log(ArLog::Normal, "Posicao atual: Pose=(%.2f,%.2f,%.2f), Trans. Vel=%.2f, Battery=%.2fV", 
		robot.getX(), robot.getY(), robot.getTh(), robot.getVel(), robot.getBatteryVoltage());
		robot.unlock();

		if (linearDesejado < abs(xFinal)*0.25) chegou = true;

	}

	// Desabilitando os motores
	robot.disableMotors();

	// Finalizando a thread do robô
	robot.stopRunning();
	
	// Esperando a thread parar
	robot.waitForRunExit();

	// Saindo do Aria
	Aria::exit();
	return 0;
}
